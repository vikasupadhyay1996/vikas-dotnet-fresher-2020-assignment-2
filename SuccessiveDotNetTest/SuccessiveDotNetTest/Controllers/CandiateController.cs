﻿using SuccessiveDotNetTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuccessiveDotNetTest.Controllers
{
    public class CandiateController : Controller
    {
        // GET: Candiate
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            InterviewScheduleDbEntities entities = new InterviewScheduleDbEntities();
            var getName = entities.Candidates.ToList();
            //Candidate can = new Candidate();


            //SelectList list = new SelectList(getName, "Id","FirstName");



            //ViewBag.nameList = list;

            ViewBag.nameList = new SelectList((from s in entities.Candidates.ToList()
                                               select new
                                               {
                                                   Id = s.Id,
                                                   FullName = s.FirstName + " " + s.LastName + "(" + s.Email + ")"
                                               }),
       "Id",
       "FullName",
       null);


            return View();
        }
        //public ActionResult Create()
        //{
        //    InterviewScheduleDbEntities entities = new InterviewScheduleDbEntities();
        //    var data = from als in entities.Candidates select new { als.FirstName, als.LastName };
        //    return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult Create(DataModel model) 
        {
            InterviewScheduleDbEntities entities = new InterviewScheduleDbEntities();
            Candidate can = new Candidate();
            can.FirstName = model.FirstName;
            can.LastName = model.LastName;
            can.Experience = model.Experience;
            can.Mobile = model.Mobile;
            can.Email = model.Email;
            entities.Candidates.Add(can);
            entities.SaveChanges();
            int latestcanid = can.Id;
            InterviewSchedule i = new InterviewSchedule();
            i.CandidateId = latestcanid;
            i.Date = model.Date;
            i.TimeFrom = model.TimeFrom;
            i.TimeTo = model.TimeTo;
            i.InterviewerName = model.InterviewerName;
            entities.InterviewSchedules.Add(i);
            entities.SaveChanges();
            return RedirectToAction("Create");


        }
        public ActionResult ScheduleInterviewDetail()
        {
            using (InterviewScheduleDbEntities db = new InterviewScheduleDbEntities())
            {
                List<Candidate> can = db.Candidates.ToList();
                List<InterviewSchedule> inter = db.InterviewSchedules.ToList();

                var employeeRecord = from e in can
                                     join d in inter on e.Id equals d.CandidateId into table1
                                     from d in table1.ToList()



                                     select new ViewModel
                                     {
                                         candidate = e,
                                         InterviewSchedule = d
                                     };
                return View(employeeRecord);
             
            }
           

        }
        public JsonResult GetStateList(int id)
        {
            InterviewScheduleDbEntities db = new InterviewScheduleDbEntities();

            db.Configuration.ProxyCreationEnabled = false;

            List<Candidate> StateList = db.Candidates.Where(x => x.Id == id).ToList();
            return Json(StateList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetData(int id)
        {
            InterviewScheduleDbEntities db = new InterviewScheduleDbEntities();
            db.Configuration.ProxyCreationEnabled = false;
            var getData = db.Candidates.Find(id);
           
            return Json(getData, JsonRequestBehavior.AllowGet);
        }

    }
}