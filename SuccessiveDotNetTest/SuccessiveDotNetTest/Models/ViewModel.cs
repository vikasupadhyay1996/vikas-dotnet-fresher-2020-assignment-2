﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuccessiveDotNetTest.Models
{
    public class ViewModel
    {
        public Candidate candidate { get; set; }
        public InterviewSchedule InterviewSchedule { get; set; }
    }
}