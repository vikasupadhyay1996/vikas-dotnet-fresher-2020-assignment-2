﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuccessiveDotNetTest.Models
{
    public class DataModel
    {
        public int Id { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]

        public int Experience { get; set; }
        [Required]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Please Enter Valid Mobile Number.")]
        public long Mobile { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        [Required]
        public int CandidateId { get; set; }
        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [Required]
        [DisplayName("From")]
        [DataType(DataType.Time),DisplayFormat(ApplyFormatInEditMode = true)]
        public TimeSpan TimeFrom { get; set; }
        [Required]
        [DisplayName("To")]
        [DataType(DataType.Time), DisplayFormat(ApplyFormatInEditMode = true)]
        public TimeSpan TimeTo { get; set; }
        [Required]
        public string InterviewerName { get; set; }
    }
}